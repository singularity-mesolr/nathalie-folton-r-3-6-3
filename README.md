# nathalie-folton-R-3-6-3

* Projet pour Nathalie Folton d'aix En provence UMR RECOVER - 11/03/2022
* Le container est une version de R en 3.6.3 (ROCKER)
* Les paquets R suivants sont installés
```bash
c("airGR","climatrends","e1071","fitdistrplus","hydroGOF","hydroTSM","maptools","parallel","plotrix","raster","zoo","DDM","hydrostats","lfstat")
```

## USAGE
Vous pouvez télécharger l'image et l'utiliser directement en tapant les commandes suivantes =>

```bash
singularity pull r-3-6-3.sif oras://registry.forgemia.inra.fr/singularity-mesolr/nathalie-folton-r-3-6-3/nathalie-folton-r-3-6-3:latest
singularity run r-3-6-3.sif file.R
```

## BUILD
Pour construire le containeur manuellement, vous pouvez utiliser la commande suivante =>
```bash
singularity build r-3-6-3.sif r-3-6-3.def
```
